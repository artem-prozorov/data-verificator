<?php

namespace Prozorov\DataVerification\Transport;

use Prozorov\DataVerification\Contracts\{TransportInterface, DestinationAddressInterface};

class DebugTransport implements TransportInterface
{
    public static $path;

    public static function setDebugPath(string $path)
    {
        static::$path = $path;
    }

    public function send(DestinationAddressInterface $address, string $text)
    {
        $filename = $this->getPath().'/'.$address->toString().'_'.strtotime('now').'.txt';

        file_put_contents($filename, $text);
    }

    protected function getPath(): string
    {
        if (empty(static::$path)) {
            return realpath(__DIR__.'/../../tests/data');
        }

        return static::$path;
    }
}
