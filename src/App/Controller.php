<?php

namespace Prozorov\DataVerification\App;

use Prozorov\DataVerification\Contracts\{DestinationAddressInterface, CodeInterface, MessageInterface};

class Controller
{
    /**
     * lockData.
     *
     * @access	public
     * @param	array                      	$data         	- the data that must be locked and verified
     * @param	DestinationAddressInterface	$address      	- address where we will send one-time password to
     * @param	MessageInterface|string   	$message        - message object or string code for the message factory
     * @return	CodeInterface
     */
    public function lockData(array $data, DestinationAddressInterface $address, $message): CodeInterface
    {
        $code = CodeManager::getInstance()->generate($address, $data);

        if (is_string($message)) {
            $message = Configuration::getInstance()->getMessageFactory()->make($message);
        }

        $message->setCode($code)->setAddress($address)->send();

        return $code;
    }

    /**
     * Unlocks the data and gets the protected data
     *
     * @access	public
     * @param	string	$verificationCode	
     * @param	string	$pass            	
     * @return	array
     */
    public function getUnlockedData(string $verificationCode, string $pass): array
    {
        $code = CodeManager::getInstance()->verify($verificationCode, $pass);

        return $code->getVerificationData();
    }

    /**
     * Returns validated data
     *
     * @access	public
     * @param	string	$verificationCode	
     * @return	array
     */
    public function getVerifiedData(string $verificationCode): array
    {
        $code = Configuration::getInstance()->getCodeRepo()->getValidatedCode($verificationCode);
        if (empty($code)) {
            throw new \OutOfBoundsException('Данные не найдены');
        }

        return $code->getVerificationData();
    }
}
