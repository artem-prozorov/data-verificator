<?php

namespace Prozorov\DataVerification\App;

use Prozorov\DataVerification\Traits\Singleton;
use Prozorov\DataVerification\Contracts\{CodeInterface, DestinationAddressInterface};
use Prozorov\DataVerification\App\Configuration;
use Prozorov\DataVerification\Exceptions\{LimitException, VerificationException};
use Prozorov\DataVerification\Models\Code;

class CodeManager
{
    use Singleton;

    public function generate(DestinationAddressInterface $address, array $data = null): CodeInterface
    {
        $this->checkCreationLimit($address);

        $verificationCode = md5(strtotime('now'));
        $code = new Code();
        $code->setVerificationCode($verificationCode)
            ->setOneTimePass($this->generateOTP())
            ->setAddress($address);

        if (!empty($data)) {
            $code->setVerificationData($data);
        }

        $code->save();

        return $code;
    }

    public function verify(string $verificationCode, string $pass): CodeInterface
    {
        $repo = Configuration::getInstance()->getCodeRepo();
        $seconds = Configuration::getInstance()->getPasswordValidationPeriod();

        $createdAfter = (new \Datetime())->sub(new \DateInterval('PT'.$seconds.'S'));
        $code = $repo->getOneUnvalidatedByCode($verificationCode, $createdAfter);
        if (!$code) {
            throw new \OutOfBoundsException('Данные не найдены');
        }

        if ($code->getOneTimePass() !== $pass) {
            $code->incrementAttempts();
            throw new VerificationException('Некорректно указан код');
        }

        if (Configuration::getInstance()->getAttempts() <= $code->getAttempts()) {
            throw new LimitException('Превышен лимит');
        }

        $code->setValidated()->save();

        return $code;
    }

    protected function generateOTP(): string
    {
        $symbols = Configuration::getInstance()->getAllowedSymbols();
        $length = Configuration::getInstance()->getPassLength();
        $otp = '';
        for ($i = 1; $i <= $length; $i++) {
            $otp .= $symbols[rand(0, (count($symbols) - 1))];
        }

        return $otp;
    }

    protected function checkCreationLimit(DestinationAddressInterface $address)
    {
        $repo = Configuration::getInstance()->getCodeRepo();

        $threshold = Configuration::getInstance()->getCreationCodeThreshold();

        $createdAfter = (new \Datetime())->sub(new \DateInterval('PT'.$threshold.'S'));

        if ($repo->getLastCodeForAddress($address, $createdAfter)) {
            throw new LimitException('Превышен лимит');
        }

        $createdAfter = (new \Datetime())->sub(new \DateInterval('PT3600S'));
        if ($attempts = $repo->getCodesCountForAddress($address, $createdAfter)) {
            $limitPerHour = Configuration::getInstance()->getLimitPerHour();
            if ($limitPerHour < $attempts) {
                throw new LimitException('Превышен лимит обращений в час');
            }
        }
    }
}
