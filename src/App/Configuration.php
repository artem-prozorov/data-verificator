<?php

namespace Prozorov\DataVerification\App;

use Prozorov\DataVerification\Traits\Singleton;
use Prozorov\DataVerification\Contracts\CodeRepositoryInterface;
use Prozorov\DataVerification\Factories\{TransportFactory, MessageFactory};

class Configuration
{
    use Singleton;

    protected $codeRepo;

    protected $allowedSymbols;

    protected $passLength = 4;

    protected $creationCodeThreshold = 60;

    protected $limitPerHour = 10;

    protected $attempts = 3;

    protected $passwordValidationPeriod = 3600;

    protected $messageFactory;

    protected $transportFactory;

    public function loadConfig(array $config = [])
    {
        if (empty($config['code_repository'])) {
            throw new \InvalidArgumentException('Укажите класс-репозиторий данных');
        }

        $this->codeRepo = new $config['code_repository'];

        $defaultTransport = ['sms' => \Prozorov\DataVerification\Transport\DebugTransport::class];
        $transportConfig = $config['transport_config'] ?? $defaultTransport;
        TransportFactory::getInstance()->loadConfig($transportConfig);

        $defaultMessages = ['sms' => \Prozorov\DataVerification\Messages\SmsMessage::class];
        $messageConfig = $config['messages'] ?? $defaultMessages;
        MessageFactory::getInstance()->loadConfig($messageConfig);

        $this->allowedSymbols = $config['allowed_symbols'] ?? range(0, 9);
        $this->passLength = $config['pass_length'] ?? 4;
        $this->creationCodeThreshold = $config['creation_code_threshold'] ?? 60;
        $this->limitPerHour = $config['limit_per_hour'] ?? 60;
        $this->attempts = $config['attempts'] ?? 3;
        $this->passwordValidationPeriod = $config['password_validation_period'] ?? 3600;
    }

    /**
     * Get the value of codeRepo
     */ 
    public function getCodeRepo(): CodeRepositoryInterface
    {
        if (empty($this->codeRepo)) {
            $this->loadConfig();
        }

        return $this->codeRepo;
    }

    /**
     * Set the value of codeRepo
     *
     * @return  self
     */ 
    public function setCodeRepo($codeRepo)
    {
        $this->codeRepo = $codeRepo;

        return $this;
    }

    public function getAllowedSymbols(): array
    {
        if (empty($this->allowedSymbols)) {
            $this->loadConfig();
        }

        return $this->allowedSymbols;
    }

    public function getPassLength(): int
    {
        return $this->passLength;
    }

    /**
     * Returns seconds threshold
     * 
     * @access	public
     * @return	integer
     */
    public function getCreationCodeThreshold(): int
    {
        return $this->creationCodeThreshold;
    }

    public function getLimitPerHour(): int
    {
        return $this->limitPerHour;
    }

    public function getAttempts(): int
    {
        return $this->attempts;
    }

    /**
     * Returns seconds threshold
     * 
     * @access	public
     * @return	integer
     */
    public function getPasswordValidationPeriod(): int
    {
        return $this->passwordValidationPeriod;
    }

    public function getTransportFactory(): TransportFactory
    {
        return TransportFactory::getInstance();
    }

    public function getMessageFactory(): MessageFactory
    {
        return MessageFactory::getInstance();
    }
}
