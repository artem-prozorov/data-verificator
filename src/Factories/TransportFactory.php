<?php

namespace Prozorov\DataVerification\Factories;

use Prozorov\DataVerification\Base\AbstractFactory;
use Prozorov\DataVerification\Traits\Singleton;

class TransportFactory extends AbstractFactory
{
    use Singleton;
}
