<?php

namespace Prozorov\DataVerification\Base;

abstract class AbstractModel
{
    protected $id = 0;

    protected $createdAt;

    /**
     * Get the value of id
     */ 
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId(int $id): AbstractModel
    {
        $this->id = $id;

        return $this;
    }

    public function getCreatedAt(): \Datetime
    {
        if (empty($this->createdAt)) {
            $this->createdAt = new \Datetime();
        }

        return $this->createdAt;
    }

    public function setCreatedAt(\Datetime $datetime): AbstractModel
    {
        $this->createdAt = $datetime;

        return $this;
    }

    public function isNew(): bool
    {
        return $this->id <= 0;
    }

    abstract public function save(): AbstractModel;
}
