<?php

namespace Prozorov\DataVerification\Models;

use Prozorov\DataVerification\Base\AbstractModel;
use Prozorov\DataVerification\Contracts\{CodeInterface, VerificationDataInterface, DestinationAddressInterface};
use Prozorov\DataVerification\App\Configuration;
use Prozorov\DataVerification\Types\Phone;

class Code extends AbstractModel implements CodeInterface
{
    protected $verificationCode;

    protected $pass;

    protected $address;

    protected $verificationData = [];

    protected $attempts = 0;

    protected $validated = false;

    public function __construct(array $data = [])
    {
        if (empty($data)) {
            return;
        }

        $this->id = $data['ID'] ?? 0;
        $this->createdAt = $data['CREATED_AT'] ?? new \DateTime();
        $this->verificationCode = $data['VERIFICATION_CODE'] ?? null;
        $this->pass = $data['PASS'] ?? null;
        $this->attempts = $data['ATTEMPTS'] ?? 0;
        $this->validated = ($data['VALIDATED'] === 'Y') ? true : false;
        $this->verificationData = $data['DATA'] ?? [];
       
        $address = $data['ADDRESS'] ?? '';
        $this->address = new Phone($address);
    }

    public function getVerificationCode(): string
    {
        return $this->verificationCode;
    }

    public function setVerificationCode(string $code): CodeInterface
    {
        $this->verificationCode = $code;

        return $this;
    }

    public function getOneTimePass(): string
    {
        return $this->pass;
    }

    public function setOneTimePass(string $pass): CodeInterface
    {
        $this->pass = $pass;

        return $this;
    }

    public function getAddress(): DestinationAddressInterface
    {
        if (empty($this->address)) {
            throw new \InvalidArgumentException('Адрес не установлен');
        }

        return $this->address;
    }

    public function setAddress(DestinationAddressInterface $address): CodeInterface
    {
        $this->address = $address;

        return $this;
    }

    public function getVerificationData(): array
    {
        return $this->verificationData;
    }

    public function setVerificationData(array $data): CodeInterface
    {
        $this->verificationData = $data;

        return $this;
    }

    public function getAttempts(): int
    {
        return $this->attempts;
    }

    public function setAttempts(int $attempts): CodeInterface
    {
        $this->attempts = $attempts;

        return $this;
    }

    public function incrementAttempts(): CodeInterface
    {
        $this->attempts++;

        return $this;
    }

    public function isValidated(): bool
    {
        return $this->validated;
    }

    public function setValidated(): CodeInterface
    {
        $this->validated = true;

        return $this;
    }

    public function save(): AbstractModel
    {
        return Configuration::getInstance()->getCodeRepo()->save($this);
    }
}
