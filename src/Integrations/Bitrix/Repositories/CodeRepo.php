<?php

namespace Prozorov\DataVerification\Integrations\Bitrix\Repositories;

use Prozorov\DataVerification\Contracts\{CodeRepositoryInterface, CodeInterface, DestinationAddressInterface};
use Prozorov\DataVerification\Integrations;
use Prozorov\DataVerification\Models\Code;
use Bitrix\Main\Type\DateTime;

class CodeRepo implements CodeRepositoryInterface
{
    /**
     * save.
     *
     * @access	public
     * @param	CodeInterface	$code	
     * @return	CodeInterface
     */
    public function save(CodeInterface $code): CodeInterface
    {
        if ($code->isNew()) {
            return $this->create($code);
        }

        return $this->update($code);
    }

    /**
     * delete.
     *
     * @access	public
     * @param	CodeRepositoryInterface	$code	
     * @return	void
     */
    public function delete(CodeRepositoryInterface $code)
    {
        if ($code->isNew()) {
            throw new \Bitrix\Main\SystemException('Не удалось удалить данные');
        }

        $result = Integrations\Bitrix\CodesTable::delete($code->getId());
        if (!$result->isSuccess()) {
            throw new \Bitrix\Main\SystemException('Не удалось удалить данные');
        }
    }

    /**
     * create.
     *
     * @access	public
     * @param	CodeInterface	$code
     * @return	CodeInterface
     */
    public function create(CodeInterface $code): CodeInterface
    {
        $result = Integrations\Bitrix\CodesTable::add([
            'VERIFICATION_CODE' => $code->getVerificationCode(),
            'ADDRESS' => $code->getAddress()->toString(),
            'PASS' => $code->getOneTimePass(),
            'ATTEMPTS' => $code->getAttempts(),
            'VALIDATED' => $code->isValidated() ? 'Y' : 'N',
            'CREATED_AT' => DateTime::createFromPhp($code->getCreatedAt()),
            'DATA' => $code->getVerificationData(),
        ]);

        if (!$result->isSuccess()) {
            throw new \Bitrix\Main\SystemException('Не удалось сохранить код');
        }

        $code->setId($result->getId());

        return $code;
    }

    /**
     * update.
     *
     * @access	public
     * @param	CodeInterface	$code	
     * @return	CodeInterface
     */
    public function update(CodeInterface $code): CodeInterface
    {
        if ($code->isNew()) {
            throw new \Bitrix\Main\SystemException('Не удалось обновить данные: указанный код не найден в БД');
        }

        $data = [
            'VERIFICATION_CODE' => $code->getVerificationCode(),
            'ADDRESS' => $code->getAddress()->toString(),
            'PASS' => $code->getOneTimePass(),
            'ATTEMPTS' => $code->getAttempts(),
            'VALIDATED' => $code->isValidated() ? 'Y' : 'N',
            'CREATED_AT' => DateTime::createFromPhp($code->getCreatedAt()),
            'DATA' => $code->getVerificationData(),
        ];

        $result = Integrations\Bitrix\CodesTable::update($code->getId(), $data);
        if (!$result->isSuccess()) {
            throw new \Bitrix\Main\SystemException('Не удалось обновить данные: '.implode(', ', $result->getErrorMessages()));
        }

        return $code;
    }

    /**
     * @access	public
     * @param	string  	$code        	
     * @param	datetime	$createdAfter	Default: null
     * @return	CodeInterface | null
     */
    public function getOneUnvalidatedByCode(string $code, \Datetime $createdAfter = null)
    {
        $params = [
            'filter' => [
                'VERIFICATION_CODE' => $code,
                'VALIDATED' => 'N',
            ],
        ];

        return $this->getData($params, $createdAfter);
    }

    /**
     * getLastCodeForAddress.
     *
     * @access	public
     * @param	destinationaddressinterface	$address     	
     * @param	datetime                   	$createdAfter	Default: null
     * @return	CodeInterface | null
     */
    public function getLastCodeForAddress(DestinationAddressInterface $address, \Datetime $createdAfter = null)
    {
        $params = [
            'filter' => [
                'ADDRESS' => $address->toString(),
                'VALIDATED' => 'N',
                'CREATED_AT' => DateTime::createFromPhp($createdAfter),
            ],
        ];

        return $this->getData($params, $createdAfter);
    }

    /**
     * getValidatedCode.
     *
     * @access	public
     * @param	string	$verificationCode	
     * @return	CodeInterface|null
     */
    public function getValidatedCode(string $verificationCode)
    {
        $params = ['filter' => [
            'VERIFICATION_CODE' => $verificationCode,
            'VALIDATED' => 'Y',
        ]];

        return $this->getData($params);
    }

    /**
     * getCodesCountForAddress.
     *=
     * @access	public
     * @param	DestinationAddressInterface	$address     	
     * @param	Datetime                   	$createdAfter	Default: null
     * @return	int | null
     */
    public function getCodesCountForAddress(DestinationAddressInterface $address, \Datetime $createdAfter = null)
    {
        $params = [
            'runtime' => [
                new \Bitrix\Main\ORM\Fields\ExpressionField('CNT', 'COUNT(*)')
            ],
            'filter' => [
                'ADDRESS' => $address->toString(),
                'VALIDATED' => 'N',
            ],
        ];

        if (!empty($createdAfter)) {
            $params['filter']['>CREATED_AT'] = $createdAfter->format('Y-m-d H:i:s');
        }

        $row = Integrations\Bitrix\CodesTable::getList($params)->fetch();
        if (empty($row)) {
            return 0;
        }

        return (int) $row['CNT'];
    }

    /**
     * getData.
     *
     * @access	protected
     * @param	array   	$params      	
     * @param	DateTime	$createdAfter	Default: null
     * @return	CodeInterface | null
     */
    protected function getData(array $params, \Datetime $createdAfter = null)
    {
        if (!empty($createdAfter)) {
            $params['filter']['>CREATED_AT'] = $createdAfter->format('Y-m-d H:i:s');
        }

        $row = Integrations\Bitrix\CodesTable::getList($params)->fetch();
        if (empty($row)) {
            return null;
        }

        $row['CREATED_AT'] = (new \Datetime())->setTimestamp($row['CREATED_AT']->getTimestamp());

        $code = new Code($row);

        return $code;
    }
}
