<?php

namespace Prozorov\DataVerification\Repositories;

use Prozorov\DataVerification\Contracts\{CodeRepositoryInterface, CodeInterface, DestinationAddressInterface};
use Prozorov\DataVerification\Integrations;
use Prozorov\DataVerification\Models\Code;

class FakeCodeRepo implements CodeRepositoryInterface
{
    public function save(CodeInterface $code): CodeInterface
    {
        return new Code();
    }

    public function delete(CodeRepositoryInterface $code)
    {
        
    }

    public function create(CodeInterface $code): CodeInterface
    {
        return new Code();
    }

    public function update(CodeInterface $code): CodeInterface
    {
        return $code;
    }

    /**
     * @access	public
     * @param	string  	$code        	
     * @param	datetime	$createdAfter	Default: null
     * @return	CodeInterface | null
     */
    public function getOneUnvalidatedByCode(string $code, \Datetime $createdAfter = null)
    {
        return null;
    }

    /**
     * getLastCodeForAddress.
     *
     * @access	public
     * @param	destinationaddressinterface	$address     	
     * @param	datetime                   	$createdAfter	Default: null
     * @return	CodeInterface | null
     */
    public function getLastCodeForAddress(DestinationAddressInterface $address, \Datetime $createdAfter = null)
    {
        return null;
    }

    /**
     * getCodesCountForAddress.
     *=
     * @access	public
     * @param	destinationaddressinterface	$address     	
     * @param	datetime                   	$createdAfter	Default: null
     * @return	int | null
     */
    public function getCodesCountForAddress(DestinationAddressInterface $address, \Datetime $createdAfter = null)
    {
        return null;
    }
}
