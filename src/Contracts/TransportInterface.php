<?php

namespace Prozorov\DataVerification\Contracts;

interface TransportInterface
{
    public function send(DestinationAddressInterface $address, string $text);
}
