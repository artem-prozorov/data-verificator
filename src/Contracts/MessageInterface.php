<?php

namespace Prozorov\DataVerification\Contracts;

interface MessageInterface
{
    public function via(string $code): MessageInterface;

    public function setCode(CodeInterface $code): MessageInterface;

    public function getCode(): CodeInterface;

    public function setTemplate(string $text): MessageInterface;

    public function getTemplate(): string;

    public function setAddress(DestinationAddressInterface $address): MessageInterface;

    public function getAddress(): DestinationAddressInterface;

    public function render(): string;

    public function send();
}
