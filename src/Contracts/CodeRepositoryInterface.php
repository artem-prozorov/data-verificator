<?php

namespace Prozorov\DataVerification\Contracts;

interface CodeRepositoryInterface
{
    public function save(CodeInterface $code): CodeInterface;

    public function delete(CodeRepositoryInterface $code);

    public function getOneUnvalidatedByCode(string $code, \Datetime $createdAfter = null);

    public function getLastCodeForAddress(DestinationAddressInterface $address, \Datetime $createdAfter = null);

    public function getCodesCountForAddress(DestinationAddressInterface $address, \Datetime $createdAfter = null);
}
