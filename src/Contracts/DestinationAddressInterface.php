<?php

namespace Prozorov\DataVerification\Contracts;

interface DestinationAddressInterface
{
    public function toString(): string;
}
