<?php

namespace Prozorov\DataVerification\Contracts;

use Prozorov\DataVerification\Base\AbstractModel;

interface CodeInterface
{
    public function getId(): int;

    public function setId(int $id): AbstractModel;

    public function getCreatedAt(): \Datetime;

    public function setCreatedAt(\Datetime $datetime): AbstractModel;

    public function getVerificationCode(): string;

    public function setVerificationCode(string $code): CodeInterface;

    public function getOneTimePass(): string;

    public function setOneTimePass(string $pass): CodeInterface;

    public function getAddress(): DestinationAddressInterface;

    public function setAddress(DestinationAddressInterface $address): CodeInterface;

    public function getVerificationData(): array;

    public function setVerificationData(array $data): CodeInterface;

    public function getAttempts(): int;

    public function setAttempts(int $attempts): CodeInterface;

    public function incrementAttempts(): CodeInterface;

    public function isValidated(): bool;

    public function setValidated(): CodeInterface;
}
