<?php

namespace Prozorov\DataVerification\Messages;

use Prozorov\DataVerification\Contracts\{MessageInterface, DestinationAddressInterface};
use Prozorov\DataVerification\App\Configuration;

class SmsMessage extends AbstractMessage
{
    public function __construct()
    {
        $this->via('sms');
    }

    protected $template = 'Здравствуйте, Ваш код подтверждения: #OTP#';
}
