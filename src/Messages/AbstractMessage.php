<?php

namespace Prozorov\DataVerification\Messages;

use Prozorov\DataVerification\Contracts\{MessageInterface, DestinationAddressInterface, CodeInterface};
use Prozorov\DataVerification\App\Configuration;

abstract class AbstractMessage implements MessageInterface
{
    protected $template;

    protected $transportCode;

    protected $address;

    protected $code;

    public function via(string $code): MessageInterface
    {
        if (! Configuration::getInstance()->getTransportFactory()->entityExists($code)) {

            throw new \InvalidArgumentException('Такой метод доставки сообщения '.$code.' недоступен');
        }

        $this->transportCode = $code;

        return $this;
    }

    public function setCode(CodeInterface $code): MessageInterface
    {
        $this->code = $code;

        return $this;
    }

    public function getCode(): CodeInterface
    {
        if (empty($this->code)) {
            throw new \InvalidArgumentException('Не установлен одноразовый пароль');
        }

        return $this->code;
    }

    public function setTemplate(string $text): MessageInterface
    {
        $this->text = $text;

        return $this;
    }

    public function getTemplate(): string
    {
        if (empty($this->template)) {
            throw new \InvalidArgumentException('Не установлен шаблон сообщения');
        }

        return $this->template;
    }

    public function setAddress(DestinationAddressInterface $address): MessageInterface
    {
        $this->address = $address;

        return $this;
    }

    public function getAddress(): DestinationAddressInterface
    {
        return $this->address;
    }

    public function send()
    {
        $text = $this->render();

        $transport = Configuration::getInstance()->getTransportFactory()->make($this->transportCode);

        $transport->send($this->getAddress(), $text);
    }

    public function render(): string
    {
        return str_replace('#OTP#', $this->getCode()->getOneTimePass(), $this->getTemplate());
    }
}
