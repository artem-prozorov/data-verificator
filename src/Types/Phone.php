<?php

namespace Prozorov\DataVerification\Types;

use Prozorov\DataVerification\Contracts\DestinationAddressInterface;

class Phone implements DestinationAddressInterface
{
    protected $phoneNum;

    public function __construct(string $phoneNum)
    {
        $this->phoneNum = preg_replace('/[^0-9]/', '', $phoneNum);
    }

    public function toString(): string
    {
        return $this->phoneNum;
    }
}
